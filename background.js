var BASIC_SITES_BLOCK_REGEX = [/doubleclick/, /chartbeat/, /scorecardresearch/, /google-analytics/, /googlesyndication/, /graph.facebook.com/, /graph-video.facebook.com/, /connect.facebook/,
  /100hot.com/, /207.net/, /247media.com/, /247realmedia.com/, /2o7.com/, /2o7.net/, /207.txt/, /7adpower.com/, /7search.com/, /8ad.com/, /911promotion.com/, /aboutblank.com/, /adbreak.com/, /adbureau.com/, /adbutler.com/, /adbutler.de/, /adbutler.net/, /addiva.com/, /addiva.net/, /addynamix.com/, /addynamix.net/, /adforce.com/, /adgoblin.com/, /admonitor.com/, /admonitor.net/, /adorigin.com/, /adorigin.net/, /ads360.com/, /ads360.net/, /adserver.com/, /adserver.txt/, /ad-slow.com/, /adtech.de/, /adtrak.net/, /adult-links.com/, /advertising.com/, /advertising.net/, /affiliate.txt/, /affiliateshop.com/, /affiliaeshop.net/, /atdmt.com/, /atdmt.net/, /atdmt.txt/, /aureate.com/, /avenuea.com/, /avertising.com/, /bankads.com/, /bannerbank.net/, /betterinternet.com/, /bfast.com/, /bfast.net/, /bizrate.com/, /bluemountain.com/, /bluestreak.txt/, /bpath.com/, /brilliantdigital.com/, /browseraidtoolbar.com/, /casalemedia.txt/, /centport.com/, /centport.net/, /cj.com/, /claria.com/, /click2net.com/, /clickagents.com/, /clickfinders.com/, /coldfushion.com/, /coldfushion.net/, /com.winactive/, /com.winactivej/, /comclick.com/, /cometcursor.com/, /cometcursor.net/, /cometcursors.com/, /cometcursors.net/, /commission-junction.com/, /commission-junction.net/, /coolweb.com/, /coolweb.net/, /coolwebsearch.com/, /coolwebsearch.net/, /coolwwwsearch.com/, /coremetrics.com/, /coremetrics.net/, /cydoor.com/, /dbbsrv.com/, /dealtime.com/, /directnetadvertising.com/, /directnetadvertising.net/, /doubleclick.co.uk/, /doubleclick.com/, /doubleclick.net/, /downloadacceleratorplus.com/, /downloadware.com/, /easyhits4u.com/, /ebch.com/, /ebdv.com/, /ebkn.com/, /ebky.com/, /eblv.com/, /ebvr.com/, /ecwz.com/, /ecye.com/, /edbw.com/, /eded.com/, /edit.com/, /eduy.com/, /engage.com/, /enliven.com/, /entk.txt/, /epilot.com/, /ezula.com/, /fastadvert.com/, /fastclick.com/, /fastclick.net/, /findwhat.com/, /flycast.com/, /flyswat.com/, /focalink.com/, /gain.com/, /gator.com/, /gator.net/, /gatoradvertisinginformationnetworks.com/, /gatoradvertisinginformationnetworks.net/, /go2net.txt/, /hightraffic.com/, /hightrafficads.com/, /hightraffics.net/, /hitbox.com/, /hitbox.net/, /hitboxcentral.com/, /hitboxcentral.net/, /hitslink.com/, /hotbar.com/, /hotlog.ru/, /hotnaughtywifes.com/, /huntbar.com/, /hyperbanner.net/, /iball.com/, /ibmx.com/, /icwb.com/, /icwo.com/, /icwp.com/, /iddh.com/, /ieaccess.com/, /ifiz.com/, /iguu.com/, /imagessaleshound.com/, /inetspeak.com/, /infinate-ads.com/, /internetfuel.com/, /kazaa.com/, /limewire.com/, /linkbuddies.com/, /links4ads.com/, /linksynergy.com/, /lop.com/, /mainentrypoint.com/, /mainentrypoint.net/, /market.net/, /marketscore.com/, /marketscore.net/, /maxcraft.com/, /maxserving.com/, /maxserving.txt/, /mediaplex.com/, /moneytree.com/, /narrowcastmebia.com/, /nnselect.com/, /offshoreclick.com/, /online-dialer.com/, /opentracker.com/, /opentracker.net/, /overture.com/, /oxcahs.com/, /partnercash.de/, /paycounter.com/, /paypopup.com/, /permedia.com/, /pointroll.com/, /popupsponser.com/, /popuptraffic.com/, /porntrack.com/, /porntracker.com/, /preferences.com/, /pstats.com/, /qksrv.com/, /qksrv.net/, /qksv.com/, /qksv.net/, /questionmarket.com/, /questionmarket.net/, /radiate.com/, /rapidblaster.com/, /realmedia.fr/, /realtracker.com/, /realtracker.net/, /revenue.com/, /revenue.txt/, /rightmedia.txt/, /roispy.com/, /ru4.com/, /s005-01-4-11-234545-68181.com/, /saleshound.com/, /samz.com/, /saoe.com/, /saveu.com/, /saveu.net/, /sbjr.com/, /sbnl.com/, /sbnt.com/, /sbvr.com/, /scdm.com/, /scrippsnet.com/, /scrippts.com/, /scrk.com/, /sdy.com/, /search.net/, /search-explorer.com/, /searchitbar.com/, /seld.com/, /servedby.com/, /servedbyadvertising.com/, /servedforvalued.com/, /servedforvaluead.com/, /sex-in-www.com/, /sexlist.com/, /sextracker.com/, /sfux.com/, /sheat.com/, /sipo.com/, /smartadserver.com/, /smartclick.com/, /smartclick.net/, /smds.com/, /specificpop.com/, /spematrix.com/, /spylog.com/, /srib.com/, /srox.com/, /srsf.com/, /ssaw.com/, /ssdy.com/, /stripplayer.com/, /superbar.com/, /surj.com/, /targetnet.com/, /tbvg.com/, /tdak.com/, /techtarget.com/, /tefs.com/, /tfil.com/, /thako.com/, /tinybar.com/, /titantv.txt/, /torc.com/, /tradedouble.com/, /trafficmarketplace.com/, /trafficmp.com/, /trafficsupport.com/, /trafficvenue.net/, /trakkerd.net/, /travelocity.com/, /tribalfusion.com/, /tribalfusion.net/, /tzko.com/, /ubbthreads.com/, /utopiad.com/, /valued.com/, /valueclick.com/, /valueclick.net.jp/, /valueclick.net/, /wbkb.com/, /webads.com/, /webtrendlive.com/, /wegcash.com/, /wegcash.net/, /wfix.com/, /wflu.com/, /wildtangent.com/, /x1.com/, /x10.com/, /xrenoder.com/, /xupiter.com/, /xxxcounter.com/, /xxxtoolbar.com/, /z ladserver.com/, /z ladserver.txt/]
var DEBUG = true;

function onrequest(req) {
    // check if request is from tracking url
    if (matchesInList(BASIC_SITES_BLOCK_REGEX, req.url)) {
        if (DEBUG) {
            console.log("Blocked site: " + req.url);
        }
        // block it
        return {
            cancel: true
        }
    }

    if (DEBUG) {
        // log what file we're going to fetch:
        console.log("Loading: " + req.method + " " + req.url + " " + req.type);
    }

    // Mask User Agent
    for (var header in req.requestHeaders) {
        if (req.requestHeaders[header].name === "User-Agent") {
            req.requestHeaders[header].value = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
        }
    }

    // // Mask Cookies
    // for (var header in req.requestHeaders) {
    //     if (req.requestHeaders[header].name === "Cookie") {
    //         req.requestHeaders[header].value = "Sold out :'(";
    //     }
    // }

    if (DEBUG) {
        // Print headers
        console.log("Headers:")
        for (var headerIndex in req.requestHeaders) {
            console.log(req.requestHeaders[headerIndex].name + " = " + req.requestHeaders[headerIndex].value);
        }
    }

    return {
        requestHeaders: req.requestHeaders
    };
}

function matchesInList(sitesRegex, url) {
    for (var regex of sitesRegex) {
        if (url.match(regex)) {
            return true;
        }
    }
    return false;
}

function onHeadersReceived(resp) {
    if (DEBUG) {
        console.log(resp);
    }
    return {
        responseHeaders: resp.responseHeaders
    };
}

// no need to change the following, it just makes sure that the above function is called whenever the browser wants to fetch a file
browser.webRequest.onBeforeSendHeaders.addListener(
    onrequest, {
        urls: ["<all_urls>"]
    }, ["blocking", "requestHeaders"]
);

browser.webRequest.onHeadersReceived.addListener(
    onHeadersReceived, {
        urls: ["<all_urls>"]
    }, ["blocking", "responseHeaders"]
);
