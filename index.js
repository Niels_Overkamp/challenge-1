const defaultState = {
    "label": "Enabled",
    "icon": "./icons/shield-32-enabled.png",
}

const differentState = {
    "label": "Disabled",
    "icon": "./icons/shield-32-disabled.png",
}


var {
    ToggleButton
} = require("sdk/ui/button/toggle");

var button = ToggleButton({
    id: "PrivacyGuard URL Blocker",
    label: defaultState.label,
    icon: defaultState.icon,
    onClick: function(state) {
        if (button.label == defaultState.label) {
            button.state(button, differentState);
        } else {
            button.state(button, defaultState);
        }
        console.log(button.state(button).label);
        console.log(button.state(button).icon);
    }
});
